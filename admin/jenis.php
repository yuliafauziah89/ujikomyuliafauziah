<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Data Barang</h2>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
				<div class="form-group">
					<a href="tambah_jenis.php"><type="button" class="btn">Tambah Data</a>
					</div>
					<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>No</th>
							<th>ID Jenis</th>
							<th>Nama Jenis</th>
							<th>Kode Jenis</th>
							<th>Keterangan</th>
							<th>Aksi</th>
						  </tr>
						</thead>
						<tbody>
                                     <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select=mysqli_query($koneksi,"select * from jenis order by id_jenis desc");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['id_jenis']; ?></td>
										<td><?php echo $data['nama_jenis']; ?></td>
										<td><?php echo $data['kode_jenis']; ?></td>
										<td><?php echo $data['keterangan']; ?></td>
                                            <td><a class="btn btn-rounded btn-inline btn-success-outline fa fa-edit" href="edit_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"></a>
                                            <a class="btn btn-rounded btn-inline btn-danger-outline fa fa-trash-o" href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"></a></td>    

                                        </tr>
                                        <?php
                                    }
                                    ?>				
                                    </tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	