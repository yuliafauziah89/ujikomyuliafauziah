
<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Barang Masuk</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Tambah Barang Masuk</h5>

				<form action="simpan_ruang.php" method="post">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Ruang</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_ruang" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kode Ruang</label>
						<div class="col-sm-10">
							<?php
							$koneksi=mysqli_connect("localhost","root","","ujikom");
							
							$cari_kd=mysqli_query($koneksi,"select max(kode_ruang)as kode from ruang");
							$tm_cari=mysqli_fetch_array($cari_kd);
							$kode=substr($tm_cari['kode'],1,4);
							$tambah=$kode+1;
							if($tambah<10){
								$kode_ruang="R000".$tambah;
							}else{
								$kode_ruang="R00".$tambah;
							}
							?>
							<p class="form-control-static"><input type="text" id="kode_ruang" name="kode_ruang" class="form-control" id="inputPassword" value="<?php echo $kode_ruang; ?>" required="harus diisi" readonly></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Keterangan</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="keterangan" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="ruang.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	