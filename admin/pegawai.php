<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Pegawai</h2>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
				<div class="form-group">
					<a href="tambah_pegawai.php"><type="button" class="btn">Tambah Data</a>
					</div>
					<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>No</th>
							<th>Id Pegawai</th>
							<th>Nama pegawai</th>
							<th>NIP</th>
							<th>Alamat</th>
							<th>Aksi</th>
						  </tr>
						</thead>
						<tbody>
                                     <?php
                                    include "../koneksi.php";
                                    $no=1;
                                    $select=mysqli_query($koneksi,"select * from pegawai order by id_pegawai desc");
                                    while($data=mysqli_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['id_pegawai']; ?></td>
										<td><?php echo $data['nama_pegawai']; ?></td>
										<td><?php echo $data['nip']; ?></td>
										<td><?php echo $data['alamat']; ?></td>
                                            <td><a class="btn btn-rounded btn-inline btn-success-outline fa fa-edit" href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"></a>
                                            <a class="btn btn-rounded btn-inline btn-danger-outline fa fa-trash-o" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"></a></td>    

                                        </tr>
                                        <?php
                                    }
                                    ?>				
                                    </tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
