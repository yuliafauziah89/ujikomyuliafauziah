<?php
include "../koneksi.php";
$id_pegawai=$_GET['id_pegawai'];

$select=mysqli_query($koneksi,"select * from pegawai where id_pegawai='$id_pegawai'");
$data=mysqli_fetch_array($select);
?>

<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Pegawai</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Horizontal Inputs</h5>

				<form action="update_pegawai.php" method="post">
				<input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai'];?>">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Pegawai</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_pegawai" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['nama_pegawai'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">NIP</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nip" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['nip'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Alamat</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="alamat" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['alamat'];?>"></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="pegawai.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	