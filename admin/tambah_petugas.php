<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Petugas</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Tambah Petugas</h5>

				<form action="simpan_petugas.php" method="post">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Username</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="username" maxlength="50" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Password</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="password" maxlength="50" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Petugas</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_petugas" maxlength="50" pattern="[A-Z a-z]+" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="email" maxlength="50" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Id Level</label>
						 <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from level order by id_level asc ");
                             $jsArray = "var id_level = new Array();\n";
                             ?> 
						<div class="col-sm-10">
							<select class="form-control m-bot15" name="id_level" required onchange="changeValue(this.value">
							 <option selected="selected">Pilih Id Level
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_level['". $row['id_level']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>                       
                            </option>
                            </select>					 
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="petugas.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
