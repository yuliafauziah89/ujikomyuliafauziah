<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pegawai</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Tambah Pegawai</h5>

				<form action="simpan_pegawai.php" method="post">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Pegawai</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_pegawai" maxlength="50" pattern="[A-Z a-z]+" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">NIP</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nip" maxlength="20" pattern="[0-9]+" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Alamat</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="alamat" maxlength="100" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="pegawai.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
