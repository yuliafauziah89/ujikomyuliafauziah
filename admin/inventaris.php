<?php
include "header.php";
?>
	
	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Inventaris</h2>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
				<div class="form-group">
						<a href="tambah_inventaris.php"><type="button" class="btn">Tambah Data</a>
						</div>
						<div class="dropdown">
                            <button class="btn btn-rounded dropdown-toggle" id="dd-header-add" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Export
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dd-header-add">
                                <a class="dropdown-item" href="export_inventaris.php">Excel</a>
                                <a class="dropdown-item" href="cetak_inventaris.php">PDF</a>
                            </div>
                        </div>
                        <br>
					<div class="table-responsive">
						<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th>No</th>
								<th>Id Inventaris</th>
								<th>Nama Barang</th>
								<th>Kondisi</th>
								<th>Spesifikasi</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
								<th>Nama Jenis</th>
								<th>Tanggal Register</th>
								<th>Nama Ruang</th>
								<th>kode inventaris</th>
								<th>Nama Petugas</th>
								<th>Sumber</th>
								<th>Aksi</th>
							  </tr>
							</thead>
							<tbody>
	                                    <?php
	                                    include "../koneksi.php";
	                                    $no=1;
	                                    $select=mysqli_query($koneksi,"select * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY id_inventaris desc");
	                                    while($data=mysqli_fetch_array($select))
	                                    {
	                                    ?>
	                                      <tr>
	                                        <td><?php echo $no++; ?></td>
	                                        <td><?php echo $data['id_inventaris']; ?></td>
	                                        <td><?php echo $data['nama']; ?></td>
											<td><?php echo $data['kondisi']; ?></td>
											<td><?php echo $data['spesifikasi']; ?></td>
											<td><?php echo $data['keterangan']; ?></td>
	                                        <td><?php echo $data['jumlah']; ?></td>
											<td><?php echo $data['nama_jenis']; ?></td>
											<td><?php echo $data['tgl_register']; ?></td>
	                                        <td><?php echo $data['nama_ruang']; ?></td>
											<td><?php echo $data['kode_inventaris']; ?></td>
											<td><?php echo $data['nama_petugas']; ?></td>
											<td><?php echo $data['sumber']; ?></td>
										
	                                            <td><a class="btn btn-rounded btn-inline btn-success-outline fa fa-edit" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a>
	                                            <a class="btn btn-rounded btn-inline btn-danger-outline fa fa-trash-o" href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a></td>    

	                                        </tr>
	                                        <?php
	                                    }
	                                    ?>								
	                                    </tbody>
						</table>
					</div>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->