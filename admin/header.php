<?php
session_start();
include '../koneksi.php';
if(!isset($_SESSION['username'])){
	echo"<script type=text/javascript>alert('Anda Belum Login');
	window.location.href='../index.php';
    </script>";
}
?>

<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inventaris</title>

	<link href="../img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="../img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="../img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="../img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="../img/favicon.png" rel="icon" type="image/png">
	<link href="../img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="../css/lib/datatables-net/datatables.min.css">
	<link rel="stylesheet" href="../css/separate/vendor/datatables-net.min.css">
    <link rel="stylesheet" href="../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/main.css">
</head>
</head>
<body class="with-side-menu">

	<header class="site-header">
	    <div class="container-fluid">
	
	        <a href="#" class="site-logo">
	            <img class="hidden-md-down" src="../img/skanic.png" alt=""><b>Inventaris</b>
	            <img class="hidden-lg-up" src="../img/skanic.png" alt="">
	        </a>
	
	        <button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
	            <span>toggle menu</span>
	        </button>
	
	        <button class="hamburger hamburger--htla">
	            <span>toggle menu</span>
	        </button>
	        <div class="site-header-content">
	            <div class="site-header-content-in">
	                <div class="site-header-shown">
	                    <div class="dropdown user-menu">
						<?php
	            	$use=$_SESSION['username'];
	            	$fo=mysqli_query($koneksi, "select nama_petugas from petugas where username ='$use'");
	            	while($f=mysqli_fetch_array($fo)) {
	            		?>
	                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <img src="../img/avatar-2-64.png" alt=""><?php echo $f['nama_petugas']; ?>
	                        </button>
	                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
	                            
	                            <a class="dropdown-item" href="../logout.php"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
	                        </div>
	                    </div>
						<?php
					}
					?>
	
	                </div><!--.site-header-shown-->
	            </div><!--site-header-content-in-->
	        </div><!--.site-header-content-->
	    </div><!--.container-fluid-->
	</header><!--.site-header-->

	<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
	    <ul class="side-menu-list">
	        <li class="blue-dirty">
	            <a href="index.php">
	                <span class="glyphicon glyphicon-th"></span>
	                <span class="lbl">Dashboard</span>
	            </a>
	        </li>
	        <li class="magenta with-sub">
	            <span>
	                <span class="glyphicon glyphicon-list-alt"></span>
	                <span class="lbl">Inventaris</span>
	            </span>
	            <ul>
	                <a href="inventaris.php"><span class="lbl">Data Barang</span></a></li>
	                <a href="jenis.php"><span class="lbl">Data Jenis</span></a></li>
	                <a href="ruang.php"><span class="lbl">Data Ruang</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <span class="font-icon font-icon-edit"></span>
	                <span class="lbl">Peminjaman</span>
	            </span>
	            <ul>
	                <a href="datatables-net.html"><span class="lbl">Data Barang yang dipinjam</span></a></li>
	                <a href="bootstrap-datatables.html"><span class="lbl">Data Barang yang sudah dikembalikan</span></a></li>
	            </ul>
	        </li>
	        <li class="blue with-sub">
	            <span>
	                <i class="font-icon font-icon-user"></i>
	                <span class="lbl">Petugas</span>
	            </span>
	            <ul>
	                <li><a href="petugas.php"><span class="lbl">Data Petugas</span></a></li>
	            </ul>
	        </li>
	        <li class="blue with-sub">
	            <span>
	                <i class="font-icon font-icon-user"></i>
	                <span class="lbl">Pegawai</span>
	            </span>
	            <ul>
	                <li><a href="pegawai.php"><span class="lbl">Data Pegawai</span></a></li>
	            </ul>
	        </li>
	        <li class="grey with-sub">
	            <a href="laporan.php">
	                <span class="glyphicon glyphicon-duplicate"></span>
	                <span class="lbl">Laporan</span>
	            </a>
	        </li>
	        <li class="blue">
	            <a href="backup.php">
	                <span class="font-icon glyphicon glyphicon-paperclip"></span>
	                <span class="lbl">Backup</span>
	            </a>
	        </li>
	</nav><!--.side-menu-->
	
	<script src="../js/lib/jquery/jquery.min.js"></script>
	<script src="../js/lib/tether/tether.min.js"></script>
	<script src="../js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>

	<script src="../js/lib/datatables-net/datatables.min.js"></script>
	<script>
		$(function() {
			$('#example').DataTable();
		});
		 
	</script>

<script src="../js/app.js"></script>
</body>
</html>