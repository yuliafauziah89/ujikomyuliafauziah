<?php
include "../koneksi.php";
$id_jenis=$_GET['id_jenis'];

$select=mysqli_query($koneksi,"select * from jenis where id_jenis='$id_jenis'");
$data=mysqli_fetch_array($select);
?>

<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Jenis</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Horizontal Inputs</h5>

				<form action="update_jenis.php" method="post">
				<input type="hidden" name="id_jenis" value="<?php echo $data['id_jenis'];?>">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Jenis</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_jenis" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['nama_jenis'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kode Jenis</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="kode_jenis" requied type="text" class="form-control" id="inputPassword" value="<?php echo $data['kode_jenis'];?>" readonly></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Keterangan</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="keterangan" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['keterangan'];?>"></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="jenis.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	