<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Inventaris</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Tambah Inventaris</h5>

				<form action="simpan_inventaris.php" method="post">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Barang</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kondisi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="kondisi" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Spesifikasi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="spesifikasi" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Keterangan</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="keterangan" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Jumlah</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="jumlah" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					
					<div class="form-group row">
					<?php
                              include "../koneksi.php";
                              $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                              $jsArray = "var id_jenis = new Array();\n";
                               ?> 
						<label class="col-sm-2 form-control-label">Id Jenis</label>
						<div class="col-sm-10">
							 <select class="form-control m-bot15" name="id_jenis" required onchange="changeValue(this.value)">
                                  <option selected="selected">Pilih Jenis
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>
                             </option>
                             </select> 
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Tanggal Register</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input name="tgl_register" type="date" class="form-control" id="inputPassword" value="<?php $tgl = Date('Y-m-d'); echo $tgl; ?>" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Id Ruang</label>
						 <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                             $jsArray = "var id_ruang = new Array();\n";
                             ?> 
						<div class="col-sm-10">
							<select class="form-control m-bot15" name="id_ruang" required onchange="changeValue(this.value">
							 <option selected="selected">Pilih Id Ruang
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>                       
                            </option>
                            </select>					 
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kode Inventaris</label>
						<div class="col-sm-10">
							<?php
							$koneksi=mysqli_connect("localhost","root","","ujikom");
							
							$cari_kd=mysqli_query($koneksi,"select max(kode_inventaris)as kode from inventaris");
							$tm_cari=mysqli_fetch_array($cari_kd);
							$kode=substr($tm_cari['kode'],1,4);
							$tambah=$kode+1;
							if($tambah<10){
								$kode_inventaris="V000".$tambah;
							}else{
								$kode_inventaris="V00".$tambah;
							}
							?>
							<p class="form-control-static"><input type="text" id="kode_inventaris" name="kode_inventaris" class="form-control" id="inputPassword" value="<?php echo $kode_inventaris; ?>" required="harus diisi" readonly></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Id Petugas</label>
						 <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                             $jsArray = "var id_petugas = new Array();\n";
                             ?> 
						<div class="col-sm-10">
							<select class="form-control m-bot15" name="id_petugas" required onchange="changeValue(this.value">
                            <option selected="selected">Pilih Id Petugas
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>
							</option>
                            </select>
						
					</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Sumber</label>
						<div class="col-sm-5">
							<p class="form-control-static"><input autocomplete="off" name="sumber" required type="text" class="form-control" id="inputPassword"></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="inventaris.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div> 
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
